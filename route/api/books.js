const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const { check, param, validationResult } = require('express-validator');

const Book = require('../../model/Book');

const ObjectIdIsValid = id => mongoose.Types.ObjectId.isValid(id) ? true : false;

// @route   GET api/books/all
// @desc    Get all list books no criteria
// @access  Public
router.get('/all', async (req, res) => {
    try {
        const book = await Book.find();

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});


// @route   GET api/books/sort/:field/:type
// @desc    Get all and sort by field
// @access  Public
router.get('/sort/:field/:type', [ 
    param('type').matches(/^(?:asc|desc)$/).withMessage('Sort type must be ASC or DESC only') 
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        let sort = {};
        let field = req.params.field === 'date' ? 'createdDate' : req.params.field;
        sort[field] = req.params.type === 'asc' ? 1 : -1;
        const book = await Book.find().sort(sort);

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/sort_price/:field/:type
// @desc    Get all and sort by price
// @access  Public
router.get('/sort_price/:field/:type', [ 
    param('field').matches(/^(?:ebook|peperwork)$/).withMessage('Price type must be Ebook or Peperwork only'),
    param('type').matches(/^(?:asc|desc)$/).withMessage('Sort type must be ASC or DESC only')
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        let sort = {};
        let field = `price.${req.params.field}`;
        sort[field] = req.params.type === 'asc' ? 1 : -1;
        const book = await Book.find().sort(sort);

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/sort_category_rating/:category/:type
// @desc    Get book by category and sort by rating
// @access  Public
router.get('/sort_category_rating/:category/:type', [
    param('type').matches(/^(?:asc|desc)$/).withMessage('Sort type must be ASC or DESC only')
], async (req, res) => {
    try {
        let sort = {};
        sort['rating'] = req.params.type === 'asc' ? 1 : -1;
        const book = await Book.find({ category: req.params.category }).sort(sort);

        if(book.length === 0) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/sort_category_rating/:category/:type
// @desc    Get book by category and sort by rating
// @access  Public
router.get('/page/:page', [
    param('page').matches(/^[0-9]*$/).withMessage('Page must be a number'),
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        const perPage = 10;
        const page = req.params.page || 1;

        const book = await Book.find().skip((perPage * page) - perPage).limit(perPage);

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/search
// @desc    Get book by search query
// @access  Public
router.get('/search', async (req, res) => {
    try {
        if(req.query !== {}) {
            let query = {};
            Object.keys(req.query).forEach(key => {
                Object.assign(query, {[key]: new RegExp(req.query[key], 'i')});
            });
            
            const books = await Book.find(query);
            
            if(books.length === 0) {
                return res.status(404).json({ msg: 'Book not found' });
            }

            res.json(books);
        }
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   POST api/books/create
// @desc    Create new book
// @access  Public
router.post('/create', [
    check('title').not().isEmpty().escape(),
    check('synoposis').not().isEmpty().escape(),
    check('isbn10').not().isEmpty().escape(),
    check('isbn13').not().isEmpty().escape(),
    check('language').not().isEmpty().escape(),
    check('publisher').not().isEmpty().escape(),
    check('edition').not().isEmpty().escape(),
    check('ebookPrice').isNumeric().escape(),
    check('peperworkPrice').isNumeric().escape(),
    check('currentAmount').isNumeric().escape(),
    check('category').not().isEmpty().escape()
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        const dupBook = await Book.findOne({ title: req.body.title });

        if(dupBook) {
            return res.status(400).json({ errors: 'This book is dupplicated' });
        }

        const newBook = {
            category: req.body.category,
            title: req.body.title,
            synoposis: req.body.synoposis,
            isbn10: req.body.isbn10,
            isbn13: req.body.isbn13,
            language: req.body.language,
            publisher: req.body.publisher,
            edition: req.body.edition,
            price: {
                ebook: req.body.ebookPrice,
                peperwork: req.body.peperworkPrice
            },
            reviews: [],
            soldAmount: 0,
            currentAmount: req.body.currentAmount
        };

        const book = await new Book(newBook).save();

        res.json(book);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   PUT api/books/update/:id
// @desc    Update book by book id
// @access  Public
router.put('/update/:id', [
    check('title').not().isEmpty().escape(),
    check('synoposis').not().isEmpty().escape(),
    check('isbn10').not().isEmpty().escape(),
    check('isbn13').not().isEmpty().escape(),
    check('language').not().isEmpty().escape(),
    check('publisher').not().isEmpty().escape(),
    check('edition').not().isEmpty().escape(),
    check('ebookPrice').isNumeric().escape(),
    check('peperworkPrice').isNumeric().escape(),
    check('currentAmount').isNumeric().escape(),
    check('category').not().isEmpty().escape()
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }

    try {
        const newBook = {
            category: req.body.category,
            title: req.body.title,
            synoposis: req.body.synoposis,
            isbn10: req.body.isbn10,
            isbn13: req.body.isbn13,
            language: req.body.language,
            publisher: req.body.publisher,
            edition: req.body.edition,
            price: {
                ebook: req.body.ebookPrice,
                peperwork: req.body.peperworkPrice
            },
            currentAmount: req.body.currentAmount
        };
        
        const book = await Book.findOneAndUpdate({_id: req.params.id}, {$set:newBook}, {new: true});

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }
        res.json(book);
    } catch (error) {
        console.log(error.message);
        return res.status(500).send('Internal Server Error!!');
    }
});

// @route   DELETE api/books/delete/:id
// @desc    Delete book by book id
// @access  Public
router.delete('/delete/:id', async (req, res) => {
    try {
        if(!ObjectIdIsValid(req.params.id)) {
            return res.status(422).json({ errors: 'Book not found' });
        }

        const book = await Book.findById(req.params.id);

        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        await book.remove();
        res.json({ msg: 'Book removed' });
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/selling/:id/:sell_amount
// @desc    Sell book params book id and sell amount
// @access  Public
router.get('/selling/:id/:sell_amount', [ param('sell_amount').isNumeric() ] , async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }
    
    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }

    try {
        const book = await Book.findById(req.params.id);
        
        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }

        if(book.currentAmount === 0) {
            return res.status(422).json({ msg: 'Sold out' });
        }
        
        if(book.currentAmount < req.params.sell_amount) {
            return res.status(422).json({ msg: 'Current is less then sell amount' });
        }

        book.soldAmount = parseInt(book.soldAmount) + parseInt(req.params.sell_amount);
        book.currentAmount = parseInt(book.currentAmount) - parseInt(req.params.sell_amount);
        book.save();
        
        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
})

// @#### Review

// @route   GET api/books/review/:id
// @desc    Get review by book id
// @access  Public
router.get('/review/:id', async (req, res) => {
    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }

    const book = await Book.findById(req.params.id);

    if(!book) {
        return res.status(404).json({ errors: 'Book not found' });
    }

    if(book.reviews.length === 0) {
        return res.status(404).json({ errors: 'Review of this book not found' });
    }

    res.json(book.reviews);
});

// @route   POST api/books/review/:id
// @desc    Create review by book id
// @access  Public
router.post('/review/:id', [
    check('score').matches(/^(?:[1-5]|0[1-5]|5)$/).withMessage('Score must be a number [1-5]'),
    check('description').not().isEmpty()
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }
    
    try {
        const book = await Book.findById(req.params.id);
        if(!book) {
            return res.status(404).json({ msg: 'Book not found' });
        }

        const newReview = {
            score: req.body.score,
            description: req.body.description
        };

        book.reviews.unshift(newReview);

        await book.save();

        book.rating = (book.reviews.reduce((acc, obj) => (acc + obj.score), 0) / book.reviews.length);
        await book.save();

        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   PUT api/books/review/:id/:review_id
// @desc    Update review by book id and review id
// @access  Public
router.put('/review/:id/:review_id', [
    check('score').matches(/^(?:[1-5]|0[1-5]|5)$/).withMessage('Score must be a number [1-5]'),
    check('description').not().isEmpty()
], async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }

    if(!ObjectIdIsValid(req.params.review_id)) {
        return res.status(422).json({ errors: 'Review not found' });
    }

    try {
        const book = await Book.findOneAndUpdate(
            { _id: req.params.id, 'reviews._id': req.params.review_id },
            { $set: {'reviews.$.score': req.body.score, 'reviews.$.description': req.body.description}},
            {new: true});
        
        if(!book) {
            return res.status(404).json({ errors: 'Book or review not found' });
        }

        book.rating = (book.reviews.reduce((acc, obj) => (acc + obj.score), 0) / book.reviews.length);
        await book.save();
        
        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   DELETE api/books/review/:id/:review_id
// @desc    Delete review by book id and review id
// @access  Public
router.delete('/review/:id/:review_id', async (req, res) => {
    if(!ObjectIdIsValid(req.params.id)) {
        return res.status(422).json({ errors: 'Book not found' });
    }

    if(!ObjectIdIsValid(req.params.review_id)) {
        return res.status(422).json({ errors: 'Review not found' });
    }

    const book = await Book.findById(req.params.id);

    if(!book) {
        return res.status(404).json({ errors: 'Book not found' });
    }

    const review = book.reviews.find(review => review.id === req.params.review_id);

    if(!review) {
        return res.status(404).json({ errors: 'Review not found' });
    }
    
    // Solution 1
    // await Book.update(
    //     {_id: req.params.id}, 
    //     {$pull: { reviews: { _id: req.params.review_id}}
    //     });
    //
    // res.json({ msg: 'Review removed' });

    // Solution 2
    const index = book.reviews.map(review => review.id).indexOf(req.params.review_id);
    book.reviews.splice(index, 1);

    await book.save();
    res.json(book.reviews);
});

// @route   GET api/books/bestseller
// @desc    Get books by best seller
// @access  Public
router.get('/bestseller', async (req, res) => {
    try {
        const book = await Book.find().sort({ soldAmount: -1 });
        if(!book) {
            return res.status(404).json({ errors: 'Book not found' });
        }
        res.json(book);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
});

// @route   GET api/books/bestseller_bycate/:category
// @desc    Get books by best seller and group by category
// @access  Public
router.get('/bestseller_bycate/:category',  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        if(req.params.category) {
            const book = await Book.find({ category: req.params.category }).sort({ soldAmount: -1 });

            if(book.length === 0) {
                return res.status(404).json({ errors: 'Book not found' });
            }
            res.json(book);
        }
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Internal Server Error!!');
    }
})

module.exports = router;