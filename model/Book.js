const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
    category: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    synopsis: {
        type: String
    },
    isbn10: {
        type: String
    },
    isbn13: {
        type: String
    },
    language: {
        type: String
    },
    publisher: {
        type: String
    },
    edition: {
        type: String
    },
    price: {
        ebook: {
            type: Number
        },
        peperwork: {
            type: Number
        }    
    },
    reviews: [{
        score: {
            type: Number
        },
        description: {
            type: String
        }
    }],
    soldAmount: {
        type: Number
    },
    currentAmount: {
        type: Number
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    rating: {
        type: Number
    }
});

module.exports = Book = mongoose.model('book', BookSchema);