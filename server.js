const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const connectDB = require('./config/db');

dotenv.config();
const app = express();

connectDB();

// Body parser middle ware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// App route middleware
const bookRoute = require('./route/api/books');

app.use('/api/books', bookRoute);

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Running on port ${port}`))